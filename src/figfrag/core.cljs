(ns ^:figwheel-hooks figfrag.core
  (:require
   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom]]
   [glslcanvas]))

(defonce app-state (atom {:sandbox nil
                          :shader "none"
                          :uniforms "none"
                          :interface-class "interface"}))

(def default-canvas "#canvas")
(def test-shader "precision mediump float; void main() { gl_FragColor = vec4(0.2, 0.2, 0.9, 1.0); }")
;;
;; wrap glslcanavs functions and add state management
;;
(defn set-sandbox
  "takes CSS selector string, sets state sandbox, returns glslCanvas obj"
  [selector]
  (let [cnv (js/document.querySelector selector)
        sandbox (glslcanvas. cnv)]
    (swap! app-state assoc :sandbox sandbox)
    sandbox))

(defn load-frag
  "takes a CSS selector and shader string, loads shader into state sandbox"
  [canvas string]
  (let [sandbox (:sandbox @app-state)]
    (.load sandbox string)))

(defn swap-uniform-string
  "concatinates history of uniforms sent into shader into state"
  [uniform & args]
  (swap! app-state update-in [:uniforms] str "> " uniform " " (str args) " \n"))

;;
;; main REPL functions
;;
(defn frag
  "Takes a glsl fragment shader string, loads it into state sandbox"
  [string]
  (swap! app-state assoc :shader string)
  (swap! app-state assoc :uniforms "")
  (load-frag default-canvas string))

(defn uniform
  "takes a uniform name and values and sends them into the current shader"
  ([uniform value]
   (swap-uniform-string uniform value)
   (.setUniform (:sandbox @app-state) uniform value))
  ([uniform value1 value2]
   (swap-uniform-string uniform value1 value2)
   (.setUniform (:sandbox @app-state) uniform value1 value2))
  ([uniform value1 value2 value3]
   (swap-uniform-string uniform value1 value2 value3)
   (.setUniform (:sandbox @app-state) uniform value1 value2 value3))
  ([uniform value1 value2 value3 value4]
   (swap-uniform-string uniform value1 value2 value3 value4)
   (.setUniform (:sandbox @app-state) uniform value1 value2 value3 value4)))

;;
;; interface
;;
(defn toggle-class [a k class1 class2]
  (if (= (@a k) class1)
    (swap! a assoc k class2)
    (swap! a assoc k class1)))

(defn get-app-element []
  (gdom/getElement "app"))

(defn canvas []
  (let [w 400
        h 400]
    [:div
     [:canvas.canvas {:id "canvas"
               :width w :height h}]
     [:div { :class (:interface-class @app-state)}
      [:button {:type "button"
                :on-click #(toggle-class app-state :interface-class "interface" "interface is-active")}  "toggle"]
      [:label
       "Fragment shader"
       [:textarea.shader {:value (:shader @app-state)
                          :readOnly true}]]
      [:label
       "Uniforms"
       [:textarea.uniforms {:value (:uniforms @app-state)
                            :readOnly true}]]]]))

;;
;; Start app!
;;
(defn mount [el]
  (reagent/render-component [canvas] el)
  (set-sandbox "#canvas"))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

(mount-app-element)

(defn ^:after-load on-reload []
  (mount-app-element)
)
