# figfrag

**Fig**wheel and **Frag**ment shaders.

This is a little prototype setup to have a REPL to 'live' code fragment shaders in the browser.

## First use

Build and prep JS dependencies
```
$ yarn install && yarn run webpack
```

## Going live

Run `$ lein fig:build` and wait for the REPL to appear.

Change namspace `=> (ns figfrag.core)` and then use the functions defined in .core to manipulate the shader, namely `frag` and `uniform`.


## Development

To get an interactive development environment run:

    lein fig:build

This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

    (js/alert "Am I connected?")

and you should see an alert in the browser window.

To clean all compiled files:

	lein clean

To create a production build run:

	lein clean
	lein fig:min

## License

Copyright © 2018 lislis

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
